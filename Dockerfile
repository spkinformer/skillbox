FROM node

RUN mkdir /skillbox1

WORKDIR /skillbox1
COPY package.json /skillbox1
RUN yarn install

COPY . /skillbox1

RUN yarn test
RUN yarn build

CMD yarn start

EXPOSE 3000
